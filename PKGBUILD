# Maintainer: Sam Burgos <santiago.burgos1089@gmail.com>
# Contributor: Kyle Laker <kyle@laker.email>

pkgname=warpinator
pkgver=1.2.4
_pkgver=debbie
pkgrel=1
pkgdesc="Allows simple local network file sharing"
arch=("x86_64")
url="http://packages.linuxmint.com/pool/backport/w/${pkgname}"
license=("GPL")
depends=(
    gtk3
    python-cryptography
    python-gobject
    python-grpcio
    python-packaging
    python-protobuf
    python-pynacl
    python-setproctitle
    python-xapp
    python-zeroconf
    xapps
)
makedepends=(
    gobject-introspection
    meson
    polkit
    python-grpcio-tools
)
optdepends=('ufw: Configure firewall rules')
conflicts=(
    lm-warp
    warpinator-git
)
source=("${pkgname}_${pkgver}.tar.xz::${url}/${pkgname}_${pkgver}+${_pkgver}.tar.xz")
sha256sums=('fe49866b47123296e2f3df7f7bbc2144b00fc410169f98fd6d001b2fef550359')

prepare() {
	cd "$srcdir/warpinator"

	# Fix hard-coded libexec dir
	sed -i 's/libexec/lib/g' \
		bin/warpinator.in \
		install-scripts/download_zeroconf.py
		sed -i 's/@libexecdir@/@libdir@/g' src/config.py.in
}

build() {
    arch-meson ${pkgname} build -Dbundle-zeroconf=false
    meson compile -C build
}

package() {
    DESTDIR="$pkgdir" meson install -C build
}
